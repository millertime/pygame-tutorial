import pygame, math

MAX_FORWARD_SPEED = 20
MAX_REVERSE_SPEED = -5
TURN_SPEED = 0.5
ACCELERATION = 0.125

class CarSprite(pygame.sprite.Sprite):

    def __init__(self, image, position):
        pygame.sprite.Sprite.__init__(self)
        self.src_image = pygame.image.load(image)
        self.position = position
        self.speed = self.direction = 0
        self.k_up = self.k_down = self.k_left = self.k_right = 0

    def update(self, deltat):
        self.speed += (self.k_up + self.k_down) * ACCELERATION
        if self.speed > MAX_FORWARD_SPEED:
            self.speed = MAX_FORWARD_SPEED
        if self.speed < MAX_REVERSE_SPEED:
            self.speed = MAX_REVERSE_SPEED

        self.direction += (self.k_right + self.k_left) * TURN_SPEED
        x, y = self.position
        rad = self.direction * math.pi / 180
        x += -self.speed * math.sin(rad)
        y += -self.speed * math.cos(rad)
        self.position = (x, y)
        self.image = pygame.transform.rotate(self.src_image, self.direction)
        self.rect = self.image.get_rect()
        self.rect.center = self.position
