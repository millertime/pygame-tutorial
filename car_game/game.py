import argparse, pygame, sys, time
from pygame.locals import *

BACKGROUND_COLOR = (0, 0, 0)
WHITE = (250, 250, 250)
ORANGE = (255, 102, 0)

NUM_LAPS = 3

from ship import ShipSprite
from pad import PadSprite
from starting_line import StartingLineSprite
from wall import Wall

screen = None
clock = None

class ShipGame():
    screen = None
    clock = None

    def __init__(self, full_screen=False):
        pygame.init()
        if full_screen:
            self.screen = pygame.display.set_mode((1024, 768), FULLSCREEN | DOUBLEBUF)
        else:
            self.screen = pygame.display.set_mode((1024,768), DOUBLEBUF)
        pygame.display.set_caption('Racing!')
        self.screen.fill(BACKGROUND_COLOR)
        font = pygame.font.Font(None, 36)
        rect = pygame.Rect((482, 5), font.size('Racing!'))
        self.title = font.render('Racing!', 1, WHITE)
        self.screen.blit(self.title, rect)

        self.start_time = time.time()
        self.laps = 0
        laps, laps_size = self.update_laps()
        laps_rect = pygame.Rect((482, 60), laps_size)
        self.screen.fill(BACKGROUND_COLOR, laps_rect.copy().inflate(50, 0))
        self.screen.blit(laps, laps_rect)
        self.ship = ShipSprite('ship.png', (896, 672))
        self.ship_group = pygame.sprite.RenderPlain(self.ship)

        # Starting line
        self.starting_line = StartingLineSprite('starting_line.png', (894, 500))
        self.starting_line_group = pygame.sprite.RenderPlain(self.starting_line)

        # Gates
        pads = [
            PadSprite(1, (900, 100)),
            PadSprite(2, (124, 100)),
            PadSprite(3, (124, 650)),
            PadSprite(4, (900, 650))
        ]
        self.current_pad = 0
        self.pad_group = pygame.sprite.RenderPlain(*pads)

        # Walls
        walls = [
            #Borders
            Wall(0, 0, 1024, 4, ORANGE),
            Wall(0, 0, 4, 768, ORANGE),
            Wall(1020, 0, 4, 768, ORANGE),
            Wall(0, 764, 1024, 4, ORANGE),

            Wall(0, 382, 512, 4, ORANGE),
            Wall(254, 190, 512, 4, ORANGE),
            Wall(766, 190, 4, 384, ORANGE),
            Wall(254, 574, 512, 4, ORANGE),
        ]
        self.wall_group = pygame.sprite.RenderPlain(*walls)

        self.clock = pygame.time.Clock()

    def event_loop(self):
        self.active = True
        while self.active:
            self.handle_user_input()
            self.render()
        else:
            self.handle_results_screen()

    def handle_user_input(self):
        for ev in pygame.event.get():
            if not hasattr(ev, 'key'):
                continue
            down = ev.type == KEYDOWN
            if ev.key == K_RIGHT:
                self.ship.k_right = down * -5
            elif ev.key == K_LEFT:
                self.ship.k_left = down * 5
            elif ev.key == K_UP:
                self.ship.k_up = down * 2
            elif ev.key == K_DOWN:
                self.ship.k_down = down * -2
            elif ev.key == K_ESCAPE:
                sys.exit(0)

    def update_timer(self):
        font = pygame.font.Font(None, 18)
        text = str(time.time() - self.start_time)
        surface = font.render(text, 1, WHITE)
        return surface, font.size(text)

    def update_laps(self):
        font = pygame.font.Font(None, 28)
        text = '%d / %d Laps Completed' % (self.laps, NUM_LAPS)
        surface = font.render(text, 1, WHITE)
        return surface, font.size(text)

    def render(self):
        deltat = self.clock.tick(60)
        def clear_solid(surface, rect):
            surface.fill(BACKGROUND_COLOR, rect)
        self.pad_group.clear(self.screen, clear_solid)
        self.wall_group.clear(self.screen, clear_solid)
        self.starting_line_group.clear(self.screen, clear_solid)
        self.ship_group.clear(self.screen, clear_solid)
        self.ship_group.update(deltat)
        pad_collisions = pygame.sprite.spritecollide(self.ship, self.pad_group, False)
        if pad_collisions:
            pad = pad_collisions[0]
            if pad.number == self.current_pad + 1:
                pad.image = pad.hit
                self.current_pad += 1
        elif self.current_pad == 4:
            for pad in self.pad_group.sprites():
                pad.image = pad.normal
            self.current_pad = 0
            self.laps += 1
            if self.laps == NUM_LAPS:
                self.done()

        # Wall collision Rect
        wall_collisions = pygame.sprite.spritecollide(self.ship, self.wall_group, False)
        if wall_collisions:
            self.ship.speed = 0

        # Wall Collision Mask, - not working yet
        #for wall in self.wall_group:
        #    ship_mask = pygame.mask.from_surface(self.ship.src_image, 127)
        #    if pygame.sprite.collide_mask(self.ship, wall):
        #        self.ship.speed = 0

        laps, laps_size = self.update_laps()
        laps_rect = pygame.Rect((482, 60), laps_size)
        self.screen.fill(BACKGROUND_COLOR, laps_rect.copy().inflate(50, 0))
        self.screen.blit(laps, laps_rect)

        timer, timer_size = self.update_timer()
        timer_rect = pygame.Rect((482, 40), timer_size)
        self.screen.fill(BACKGROUND_COLOR, timer_rect.copy().inflate(20, 0))
        self.screen.blit(timer, timer_rect)

        self.pad_group.draw(self.screen)
        self.wall_group.draw(self.screen)
        self.starting_line_group.draw(self.screen)
        self.ship_group.draw(self.screen)
        pygame.display.flip()

    def done(self):
        big_font = pygame.font.Font(None, 48)
        finish_surface = big_font.render('Finished!', 1, WHITE)
        finish_rect = pygame.Rect((450, 350), big_font.size('Finished!'))
        self.screen.blit(finish_surface, finish_rect)
        font = pygame.font.Font(None, 36)
        press_enter_surface = font.render('Press ENTER to quit', 1, WHITE)
        press_enter_rect = pygame.Rect((410, 400), font.size('Press ENTER to quit'))
        self.screen.blit(press_enter_surface, press_enter_rect)
        self.active = False

    def handle_results_screen(self):
        while True:
            for ev in pygame.event.get():
                if hasattr(ev, 'key') and ev.key == K_RETURN:
                    sys.exit(0)

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--full-screen', action='store_true')
    args = parser.parse_args()

    game = ShipGame(args.full_screen)
    game.event_loop()

if __name__ == "__main__":
    main()
