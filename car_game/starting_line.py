import pygame

class StartingLineSprite(pygame.sprite.Sprite):
    def __init__(self, number, position):
        pygame.sprite.Sprite.__init__(self)
        self.normal = pygame.image.load('starting_line.png')
        self.rect = pygame.Rect(self.normal.get_rect())
        self.rect.center = position
        self.image = self.normal
