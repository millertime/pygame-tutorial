PyGame Tutorial
===============

Slightly modified version of Richard Jones' game from his [tutorial](http://richard.cgpublisher.com/product/pub.84/prod.11).

### Setup

* Visit the PyGame [downloads](http://pygame.org/download.shtml) page and install PyGame.
* Run `python game.py`
